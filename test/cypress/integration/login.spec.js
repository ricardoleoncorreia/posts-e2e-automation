'use strict';

describe('Login test', () => {
  before(() => {
    cy.exec('npm run test:clean');
  });

  beforeEach(() => {
    cy.fixture('user.json').as('userData');
    cy.visit('/login');
    cy.contains('h1', 'Bienvenido').should('be.visible');
  });

  it('should register an user', () => {
    cy.get('@userData').then((userData) => {
      cy.createUser(userData);
      cy.screenshot('register-user');
    });
  });

  it('should fail with a wrong user', () => {
    cy.loginUser('fail@test.com', 'test1234');

    cy.get('.error-msg').should('be.visible');

    cy.screenshot('login-failed', { blackout: ['#email1'] });
  });

  it('should login an user', () => {
    cy.get('@userData').then((userData) => {
      cy.loginUser(userData.email, userData.password);

      cy.contains('a', 'Dashboard').should('be.visible');

      cy.screenshot('login-user');
    });
  });

  after(() => {
    cy.log('Tests finished!');
  });
});
