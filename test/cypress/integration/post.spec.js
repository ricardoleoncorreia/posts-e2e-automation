'use strict';

describe('Posts test', () => {
  before(() => {
    cy.exec('npm run test:clean');
    cy.fixture('user.json').as('userData');
    cy.visit('/login');

    cy.get('@userData').then((userData) => {
      cy.createUser(userData);
      cy.visit('/dashboard');
      cy.wait(3000);
    });
  });

  it('should register an user', () => {
    cy.get('@userData').then((userData) => {
      const postContent = Cypress.env('postContent');

      cy.get('textarea').type(postContent);

      cy.contains('button', 'Crear').as('createButton');
      cy.get('@createButton').should('be.enabled');
      cy.get('@createButton').click();

      cy.contains('.col2 h5', userData.name).should('be.visible');
      cy.contains('p', postContent).should('be.visible');

      cy.screenshot('post-created');
    });
  });

  after(() => {
    cy.log('Tests finished!');
  });
});
