# PlatziPosts

This project implements the foundamentals of end-to-end testing with Cypress. It is based on a Vue application provided by [Platzi](https://platzi.com/) in its [Cypress](https://platzi.com/cursos/testing-cypress/) course.

### Installation

In order to follow the course and use the provided application, it is required to install old versions for Node and npm packages.

1. Install NVM in the local machine.
1. Run `nvm install 8` to install NodeJS v8.
1. Change the Node version with `nvm use 8`.
1. Run `npm install` to install all packages required to run the application.
1. Create an project in [Firebase](https://firebase.google.com/) and set up the authentication feature.
1. Create in the `config` folder a `test.env` and a `firebase-admin-keys.json` files. The first one to add the credentials to allow the application to write in the database (use `example-env` as an example) and the second one admin permissions to clean the database.
1. Execute `npm run cypress:open` to start the Cypress server.

### Documentation

#### Frameworks

- [ChaiJS](https://www.chaijs.com/).
- [MochaJS](https://mochajs.org/).

#### Cypress

- [Installation](https://docs.cypress.io/guides/getting-started/installing-cypress#System-requirements).
- [Test Runner](https://docs.cypress.io/guides/core-concepts/test-runner#Overview).
- [Bundled Tools](https://docs.cypress.io/guides/references/bundled-tools#Mocha).
- [Variables and Aliases](https://docs.cypress.io/guides/core-concepts/variables-and-aliases).
- [Fixtures](https://docs.cypress.io/api/commands/fixture#Syntax).
- [exec command](https://docs.cypress.io/api/commands/exec#Syntax).
- [task command](https://docs.cypress.io/api/commands/task#Syntax).
- [Custom Commands](https://docs.cypress.io/api/cypress-api/custom-commands#Syntax).
- [Environment Variables](https://docs.cypress.io/guides/guides/environment-variables).
- [Screenshots](https://docs.cypress.io/api/commands/screenshot).
- [Stubs, Spies, and Clocks](https://docs.cypress.io/guides/guides/stubs-spies-and-clocks).
- [Debugger](https://docs.cypress.io/guides/guides/debugging#Using-debugger).
- [Command Line](https://docs.cypress.io/guides/guides/command-line#Commands).
- [Conditional Testing](https://docs.cypress.io/guides/core-concepts/conditional-testing).
- Plugins [directory](https://docs.cypress.io/plugins/directory) and [Tooling](https://docs.cypress.io/guides/tooling/plugins-guide).
- [Web Security](https://docs.cypress.io/guides/guides/web-security).
- [Cypress Dashboard](https://www.cypress.io/dashboard/).

### License

Copyright 2018 Savvy Apps
Modifications Copyright (C) 2018 Platzi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
